#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
static  const  char *dirpath = "/home/zufarrifqi/Documents";
const char* prefix = "Animeku_";
const char* logpath = "/home/zufarrifqi/Wibu.log";
static int NAM_DO_SAQ = 2;
static char pathtiga[1024];
 
void convertBinerToDecimal(char decimal[], char binary[])
{
    unsigned long long int decimalValue = 0;
    int length = strlen(binary);
    for (int i = 0; i < length; i++)
    {
        decimalValue <<= 1;
        decimalValue += binary[i] - '0';
    }
    sprintf(decimal, "%lld", decimalValue);
}
 
void convertDecimalToBinary(char binary[], char decimal[])
{
    unsigned long long int decimalValue = 0;
    int length = strlen(decimal);
    for (int i = 0; i < length; i++)
    {
        decimalValue *= 10;
        decimalValue += decimal[i] - '0';
    }
 
    length = 0;
    for (length = 0; decimalValue > 0; length++)
    {
    binary[length] = decimalValue % 2 + '0';
    decimalValue >>= 1;
    }
 
    char temp;
    for (int i = 0; i < length/2; i++)
    {
    temp = binary[i];
    binary[i] = binary[length - 1 - i];
    binary[length - 1 - i] = temp;
    }
 
    binary[length] = '\0';
}
void decryptText(char * str, int startIndex, int endIndex, int encryptionType) {
    if (encryptionType == NAM_DO_SAQ) {
        if (endIndex != -1) {
            char binary[128];
            convertDecimalToBinary(binary, str + endIndex);
            int length = strlen(binary);
            for (int i = length - 1; i >= 0; i--) {
                if (binary[i] == '1') {
                    str[startIndex] = str[startIndex] - 'A' + 'a';
                }
                startIndex--;
            }
        }
    }
}

void decryptFile(char * str, int startIndex, int endIndex, int encryptionType) {
    int fileExtensionPos = endIndex - 1;
    if (encryptionType == NAM_DO_SAQ) {
        int fileExtensionPosDecimalCode = endIndex - 1;
        while (fileExtensionPosDecimalCode >= startIndex && str[fileExtensionPosDecimalCode] != '.') {
            fileExtensionPosDecimalCode--;
        }

        fileExtensionPos = fileExtensionPosDecimalCode - 1;
        while (fileExtensionPos >= startIndex && str[fileExtensionPos] != '.') {
            fileExtensionPos--;
        }

        if (fileExtensionPosDecimalCode < startIndex) {
            decryptText(str, startIndex, -1, encryptionType);
        } else if (fileExtensionPos < 0) {
            decryptText(str, fileExtensionPosDecimalCode - 1, fileExtensionPosDecimalCode + 1, encryptionType);
        } else {
            decryptText(str, fileExtensionPos - 1, fileExtensionPosDecimalCode + 1, encryptionType);
            str[fileExtensionPosDecimalCode] = '\0';
        }
    }
}

void decodeDirectoryPath(char * path, int offset, int length, int encryptionType) {
    char * slashPos = NULL;

    if (offset < length) {
        if ((slashPos = strstr(path + offset, "/")) != NULL) {
            if (encryptionType == NAM_DO_SAQ) {
                decryptText(path, offset, -1, encryptionType);
            } else {
                decryptText(path, offset, slashPos - path, encryptionType);
            }
        } else {
            decryptFile(path, offset, length, encryptionType);
        }
    }
}

int getEncryptionType(const char * path, int * offset) {
    int encryptionType = -1;
    if (strncmp(path + * offset, "/nam_do-saq_", 12) == 0) {
        encryptionType = NAM_DO_SAQ;
    }

    char * pos = strstr(path + * offset + 1, "/");
    if (pos != NULL) {
        * offset = pos - path;
    } else {
        * offset = -1;
    }
    return encryptionType;
}

int decodePath(char * filePath,
    const char * path) {
    int encryptionType = -1;
    int pathStringLength = strlen(path);

    if (strcmp(path, "/") == 0) {
        sprintf(filePath, "%s", pathtiga);
    } else {
        int offset = 0;
        while (offset != -1) {
            decodeDirectoryPath(path, offset + 1, pathStringLength, encryptionType);

            int nextEncryption = getEncryptionType(path, & offset);
            if (encryptionType == -1 || nextEncryption != -1) {
                encryptionType = nextEncryption;
            }
        }
        sprintf(filePath, "%s%s", pathtiga, path);
    }

    return encryptionType;
}

void decodeDirectoryForRename(const char * filePath,
    const char * path) {
    char tempPath[1024];
    char tempPath2[1024];
    strcpy(tempPath, path);
    int slashFinder = strlen(tempPath) - 1;
    while (tempPath[slashFinder] != '/' && slashFinder >= 0) {
        slashFinder--;
    }
    if (slashFinder >= 0) {
        tempPath[slashFinder] = '\0';
    }

    decodePath(tempPath2, tempPath);
    strcat(tempPath, path + slashFinder);
    strcat(tempPath2, path + slashFinder);
    strcpy(filePath, tempPath2);
    strcpy(path, tempPath);
}

static int fuse_mkdir(const char * path, mode_t mode) {
    char filePath[1024];
    decodeDirectoryForRename(filePath, path);

    writeLog("INFO", "MKDIR", path, "");

    return mkdir(filePath, mode | S_IFDIR);
}

static int fuse_rmdir(const char * path) {
    char filePath[1024];
    decodePath(filePath, path);

    writeLog("WARNING", "RMDIR", path, "");

    return rmdir(filePath);
}
void createLog(char *from,char *to,bool isEncrypted){
    time_t rawtime;
    time(&rawtime);
    char textLog[1000];
    FILE *file;
    file = fopen(logpath, "a");
    if(isEncrypted)
        sprintf(textLog, "RENAME\tterenkripsi\t%s\t-->%s\n", from, to);
    else
        sprintf(textLog, "RENAME\tterdecode\t%s\t-->%s\n", from, to);
    fputs(textLog, file);
    fclose(file);
    return;
}

//get the first path
int getFirstIndex(char* path)
{
    int count = 0;
    for (int i = 0; i < strlen(path); i++)
    {
        if (path[i] == '/')
        {
            count++;
        }
        if (count == 1)
        {
            return i;
        }
    }
    return strlen(path);
}
void decode(char *str){
    int i;
    bool isExt = false;
    
    int lastIndex = strlen(str);
    int index = getFirstIndex(str);
    for (i = index; i < lastIndex ; i++){
        if(str[i] == '/') {
            isExt = false;
            continue;
        }
        if(str[i] == '.')
        { 
            isExt = true;
            continue;
        }
        if(isExt)
            continue;
        if (str[i] >= 'a' && str[i] <= 'm') str[i] += 13; 
        else if (str[i] >= 'n' && str[i] <= 'z') str[i] -= 13;
        else if (str[i] >= 'A' && str[i] <= 'Z')  
            str[i] = 'A' + (('Z' - str[i]) % 26);
    }
}
void encode(char *str){
    int i;
    bool isExt = false;
    
    int lastIndex = strlen(str);
    int index = getFirstIndex(str);
    if(index == lastIndex)
        index = 0;
    for (i = index; i < lastIndex ; i++){
        if(str[i] == '/') {
            isExt = false;
            continue;
        }
        if(str[i] == '.')
        { 
            isExt = true;
            continue;
        }
        if(isExt)
            continue;
        if (str[i] >= 'a' && str[i] <= 'm') str[i] += 13; 
        else if (str[i] >= 'n' && str[i] <= 'z') str[i] -= 13;
        else if (str[i] >= 'A' && str[i] <= 'Z')  
            str[i] = 'A' + (('Z' - str[i]) % 26);
    }
    printf("%s\n", str);
}

// PART 2 STARTS HERE
int vigenereCounter=0;
int vigenereKey[11]={8,13,13,20,6,0,13,19,4,13,6};

char vigenere(char input, int mode){
    int buf = (int)input;
    int lowOrUp;

    if(buf>=65 && buf<=90){
        lowOrUp=1;
        buf -= 65;
    }else if(buf>=97 && buf<=122){
        lowOrUp=0;
        buf -= 97;
    }else{
        return (char)buf;
    }

    if(mode == 1){
        buf += vigenereKey[vigenereCounter];
        buf = buf%26;
    }else{
        if((buf - vigenereKey[vigenereCounter])<0) buf += 26;
        buf -= vigenereKey[vigenereCounter];
    }

    vigenereCounter++;
    if(vigenereCounter >= 11) vigenereCounter = 0;

    if(lowOrUp) return (char)(buf + 65);
    else return (char)(buf + 97);
}

void vigenereFile(char *pathToDir,char *filename, int mode){
    FILE *fp1, *fp2;
    char filesrc[2048], filedup[2048],buf[1024];
    char c;
    int len = strlen(filename);


    for(int i=0;i<len;i++){
        buf[i] = vigenere(filename[i],mode);
    }

    sprintf(filesrc,"%s%s",pathToDir,filename);
    sprintf(filedup,"%s%s",pathToDir,buf);

    fp1 = fopen(filesrc, "r");
    if(fp1 == NULL){
        puts("Cannot open converting file.");
        exit(EXIT_FAILURE);
    }

    fp2 = fopen(filedup, "w");
    if(fp1 == NULL){
        puts("Cannot create duplicate file.");
        exit(EXIT_FAILURE);
    }

    c = vigenere(fgetc(fp1),mode);
    while(c != EOF){
        fputc(c, fp2);
        c = vigenere(fgetc(fp1),mode);
    }

    puts("Conversion successfull");
    
    fclose(fp1);
    fclose(fp2);    
    remove(filesrc);
}

void logContent(char *cmd, char *desc, int infoOrWarning) 
{
    char filelogpath[1024],username[64];
    getlogin_r(username,64);
    sprintf(filelogpath,"/home/%s/hayolongapain_I01.log",username);
    time_t t = time(NULL);
    struct tm* lt = localtime(&t);
    char timestamp[30];
    strftime(timestamp, 30, "%d%m%Y-%H:%M:%S", lt);
    char content[1100];
    if(infoOrWarning){
        sprintf(content, "INFO::%s:%s::%s", timestamp, cmd, desc);
    }else{
        sprintf(content, "WARNING::%s:%s::%s", timestamp, cmd, desc);
    }
    FILE *fp = fopen(filelogpath, "a");
    fputs(content,fp);
    fclose(fp);
    return;
}

void systemLog(char *path1, char *path2, int mode) 
{
    char buff[1024], cmd[32];
    if(path2[0]!='\0' && mode == 1){
        strcpy(cmd, "RENAME");
        sprintf(buff, "%s::%s", path1, path2);
        logContent(cmd,buff,1);
    }else{
        if(mode == 2){
            strcpy(cmd, "MKDIR");
            sprintf(buff, "%s", path1);
            logContent(cmd, buff,1);
        }else if(mode == 3){
            strcpy(cmd, "RMDIR");
            sprintf(buff, "%s", path1);
            logContent(cmd, buff,0);
        }else if(mode == 4){
            strcpy(cmd, "UNLINK");
            sprintf(buff, "%s", path1);
            logContent(cmd, buff,0);
        }
    } 
    
}

int isADir(const char *path)
{
    struct stat path_stat;
    stat(path, &path_stat);
    return S_ISDIR(path_stat.st_mode);
}

void convertDirContentVigenere(char *pathToDir, int mode){
    DIR* FD;
    struct dirent* in_file;

    if (NULL == (FD = opendir (pathToDir))) 
    {
        fprintf(stderr, "Error : Failed to open input directory - %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    while ((in_file = readdir(FD))) 
    {
        if (!strcmp (in_file->d_name, "."))
            continue;
        if (!strcmp (in_file->d_name, ".."))    
            continue;
        vigenereFile(pathToDir,in_file->d_name,mode);
    }
}

int checkIAN(char *dirpath){
    char delim[] = "/";
    char *last;
    char *ptr = strtok(dirpath, delim);
    while(ptr != NULL)
    {
        last = ptr;
        ptr = strtok(NULL, delim);
    }
    if(strncmp(last,"IAN_",4) == 0)return 1;
    else return 0;
}
// PART 2 ENDS HERE

static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];
    char *wibu = strstr(path, prefix);
    if(wibu)
    {
        decode(wibu);
    }
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    }
    else
    {
        sprintf(fpath,"%s%s",dirpath,path);
    }

    res = lstat(fpath, stbuf);
    //deencode after animeku
    if (res == -1) return -errno;
    return 0;
}


static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    char *wibu = strstr(path, prefix);
    char filePath[1024];
    int encryptionType = decodePath(filePath, path);
    if(wibu)
    {
        decode(wibu);
    }
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);
    // printf("%s\n",fpath);

    int res = 0;
    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;
    // printf("%s\n",path);
    dp = opendir(fpath);
    

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        char filepath[2000];
        memset(filepath, 0, sizeof(filepath));
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        
        
        char newfile[500];
        sprintf(filepath, "%s/%s", fpath, de->d_name);
        if(wibu) encode(de->d_name);
        strcpy(fileName, filePath);
        strcat(fileName, "/");
        strcat(fileName, dirData->d_name);
        if (isRegularFile(fileName))
        {
            strcpy(fileName, dirData->d_name);
            encryptFile(fileName, 0, strlen(fileName), encryptionType);
        }
        else{
            strcpy(fileName, dirData->d_name);
            if (encryptionType != NAM_DO_SAQ)
            {
                encryptText(fileName, 0, strlen(fileName), encryptionType);
            }
        }

        // printf("%s\n",de->d_name);
        res = (filler(buf, de->d_name, &st, 0));
        if(res!=0) break;
    }

    closedir(dp);

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    char *wibu = strstr(path, prefix);
    if(wibu)
    {
        decode(wibu);
    }
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;

        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);
    // printf("%s\n",fpath);
    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}

//add rename
static int xmp_rename(const char *from, const char *to)
{
    int res;
    char ffrom[1000];
    char fto[1000];
    char *wibu = strstr(to, prefix);
    bool isEncrypted= false;
    strcpy(ffrom,dirpath);
    strcat(ffrom,from);
    strcpy(fto,dirpath);
    strcat(fto,to);
    res = rename(ffrom,fto);
    if(wibu)
    {
        isEncrypted = true;
        createLog(ffrom,fto,isEncrypted);
    }
    else
        createLog(ffrom,fto,isEncrypted);
    int isIANFrom, isIANTo;
    isIANFrom = checkIAN(ffrom);
    isIANTo = checkIAN(fto);
    if(isIANFrom || isIANTo){
        // if(isIANFrom == isIANTo || isADir(pathToDir) == 0){
        //     break;
        // }else 
        if(isIANTo){
            convertDirContentVigenere(ffrom,1);
        }else{
            convertDirContentVigenere(fto,0);
        }
        systemLog(ffrom,fto,1);
    }
    
    if (res == -1) return -errno;
    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .rename = xmp_rename,
    .mkdir = fuse_mkdir,
    .rmdir = fuse_rmdir,
};



int  main(int  argc, char *argv[])
{
    umask(0);

    return fuse_main(argc, argv, &xmp_oper, NULL);
}
