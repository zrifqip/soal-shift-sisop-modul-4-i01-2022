# soal-shift-sisop-modul-4-I01-2022

Group Members:

1. Denta Bramasta Hidayat (5025201116)
2. Muhammad Fatih Akbar (5025201117)
3. Muhammad Zufarrifqi Prakoso (5025201276)

# Important Links
[Questions](https://docs.google.com/document/d/13HAYs1Dp9-gZ0RtFUQqf-SULoj4dL9JG0LoiKgrOY7Y/edit)
[Answer](https://gitlab.com/zrifqip/soal-shift-sisop-modul-4-i01-2022/-/blob/main/anya_i01.c)

# Table Of Contents

# Coding

# Soal 1
## Question

a. Semua direktori dengan awalan “Animeku_” akan terencode dengan ketentuan semua file yang terdapat huruf besar akan ter encode dengan atbash cipher dan huruf kecil akan terencode dengan rot13
    
b. Semua direktori di-rename dengan awalan “Animeku_”, maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan sama dengan 1a.
c. Apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode.
d. Setiap data yang terencode akan masuk dalam file “Wibu.log” 
Contoh isi: 
RENAME terenkripsi /home/[USER]/Downloads/hai --> /home/[USER]/Downloads/Animeku_hebat 
RENAME terdecode /home/[USER]/Downloads/Animeku_ --> /home/[USER]/Downloads/Coba
e. Metode encode pada suatu direktori juga berlaku terhadap direktori yang ada di dalamnya.(rekursif)

## Explanation
### 1A. Files in Directory with "Animeku_" Prefix will be encoded with atbash if uppercase and rot13 if lowercase
First we have to made the encoder and decoder  for the path 
```c
void encode(char *str){
    int i;
    bool isExt = false;
    
    int lastIndex = strlen(str);
    int index = getFirstIndex(str);
    if(index == lastIndex)
        index = 0;
    for (i = index; i < lastIndex ; i++){
        if(str[i] == '/') {
            isExt = false;
            continue;
        }
        if(str[i] == '.')
        { 
            isExt = true;
            continue;
        }
        if(isExt)
            continue;
        if (str[i] >= 'a' && str[i] <= 'm') str[i] += 13; 
        else if (str[i] >= 'n' && str[i] <= 'z') str[i] -= 13;
        else if (str[i] >= 'A' && str[i] <= 'Z')  
            str[i] = 'A' + (('Z' - str[i]) % 26);
    }
    printf("%s\n", str);
}
void decode(char *str){
    int i;
    bool isExt = false;
    
    int lastIndex = strlen(str);
    int index = getFirstIndex(str);
    for (i = index; i < lastIndex ; i++){
        if(str[i] == '/') {
            isExt = false;
            continue;
        }
        if(str[i] == '.')
        { 
            isExt = true;
            continue;
        }
        if(isExt)
            continue;
        if (str[i] >= 'a' && str[i] <= 'm') str[i] += 13; 
        else if (str[i] >= 'n' && str[i] <= 'z') str[i] -= 13;
        else if (str[i] >= 'A' && str[i] <= 'Z')  
            str[i] = 'A' + (('Z' - str[i]) % 26);
    }
}
```
in those two function first to encode is that we get the index after the slash and then for the last index we take the index before the file extensions we need. next we use fuse for reading the directory, getting the file path and to encode the file.
```c
static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];
    char *wibu = strstr(path, prefix);
    if(wibu)
    {
        decode(wibu);
    }
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    }
    else
    {
        sprintf(fpath,"%s%s",dirpath,path);
    }

    res = lstat(fpath, stbuf);
    //deencode after animeku
    if (res == -1) return -errno;
    return 0;
}
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    char *wibu = strstr(path, prefix);
    if(wibu)
    {
        decode(wibu);
    }
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;

        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);
    // printf("%s\n",fpath);
    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}
```
in read and get attribute fuse function we decode first to read and get attribute of the real file 
```c
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    char *wibu = strstr(path, prefix);
    if(wibu)
    {
        decode(wibu);
    }
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);
    // printf("%s\n",fpath);

    int res = 0;
    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;
    // printf("%s\n",path);
    dp = opendir(fpath);
    

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        char filepath[2000];
        memset(filepath, 0, sizeof(filepath));
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        
        
        char newfile[500];
        sprintf(filepath, "%s/%s", fpath, de->d_name);
        if(wibu) encode(de->d_name);
        // printf("%s\n",de->d_name);
        res = (filler(buf, de->d_name, &st, 0));
        if(res!=0) break;
    }

    closedir(dp);

    return 0;
}
```
in readdir we check the directory if it has "Animeku_" prefix and then we encode it.

### 1B. encode file in directory if renamed directory have "Animeku_" Prefix
we use rename fuse so the we can rename the directory
```c
static int xmp_rename(const char *from, const char *to)
{
    int res;
    char ffrom[1000];
    char fto[1000];
    char *wibu = strstr(to, prefix);
    bool isEncrypted= false;
    strcpy(ffrom,dirpath);
    strcat(ffrom,from);
    strcpy(fto,dirpath);
    strcat(fto,to);
    res = rename(ffrom,fto);
}
```
and then if we rename the file it automatically the file will automatically encoded

### 1C. decode file in directory if the renamed directory doesnt have "Animeku_" Prefix
same as 1b it will decoded automatically
### 1D. Encoded data will be printed to Wibu.log
in fuse rename we check first if the file directory have Animeku_ prefix
```c
char *wibu = strstr(to, prefix);
if(wibu){
    isEncrypted = true;
    createLog(ffrom,fto,isEncrypted);
}
else createLog(ffrom,fto,isEncrypted);
```
and then we use `createlog` to make log file
```c
void createLog(char *from,char *to,bool isEncrypted){
    time_t rawtime;
    time(&rawtime);
    char textLog[1000];
    FILE *file;
    file = fopen(logpath, "a");
    if(isEncrypted)
        sprintf(textLog, "RENAME\tterenkripsi\t%s\t-->%s\n", from, to);
    else
        sprintf(textLog, "RENAME\tterdecode\t%s\t-->%s\n", from, to);
    fputs(textLog, file);
    fclose(file);
    return;
}
```
if the file have "Animeku_" prefix it will print Terenkripsi otherwise it will print terdecode.
### 1E. Directory encode method will be apply to all directory inside of it
THis encoder will be applied to all of the directory
## Example
Encoded File
![en](https://i.imgur.com/SI9KX6c.png)
Renamed Directory
![po](https://i.imgur.com/3eVNGK4.png)
Decoded File
![fs](https://i.imgur.com/9hNVtha.png)
Log File
![log](https://cdn.discordapp.com/attachments/882999285539541014/975368848041726022/Screenshot_from_2022-05-15_19-05-11.png)


## Revision and Difficulties
+ the program can encode the file
- cant apply recursion for the encoder but still worked just fine

# Soal 2

## Question
>Saat Anya sedang sibuk mengerjakan programnya, tiba-tiba Innu datang ke rumah Anya untuk mengembalikan barang yang dipinjamnya. Innu adalah programmer jenius sekaligus teman Anya. Ia melihat program yang dibuat oleh Anya dan membantu Anya untuk menambahkan fitur pada programnya dengan ketentuan sebagai berikut ...

The question then explains further that the following things are to be made:

+ A directory contents would either be encoded/decoded depending on if the directory name starts with or without "IAN_", If a file is renamed from without to using "IAN_" then its content would be decoded, and vice versa.
+ The encoding would use a [Vigenère Cipher](https://www.dcode.fr/vigenere-cipher) using the key "INNUGANTENG", this would encode the file's name and content.
+ For additional securities, a log file with the path `home/[user]/hayolongapain_I01.log`is made that will contain the following logs:
  + For *rmdir* or *unlink* commands, the log would be indicated with a `WARNING` label.
  + For other commands, the log would be indicated with an `INFO` label.
  + Other than that, the format of the logs are more or less the same as in `[Level]::[dd][mm][yyyy]-[HH]:[MM]:[SS]::[CMD]::[DESC :: DESC]`

## Explanation

### 2a. Vigenère Cipher Encoding & Decoding

In short, a Vigenère Cipher is a way of ciphering a string of text by shifting the alphabet a number of steps (like that of the [Caesar Cipher](https://en.wikipedia.org/wiki/Caesar_cipher)) but with the addition of a key for further processing.

```c
int vigenereCounter=0;
int vigenereKey[11]={8,13,13,20,6,0,13,19,4,13,6};

char vigenere(char input, int mode){
    int buf = (int)input;
    int lowOrUp;

    if(buf>=65 && buf<=90){
        lowOrUp=1;
        buf -= 65;
    }else if(buf>=97 && buf<=122){
        lowOrUp=0;
        buf -= 97;
    }else{
        return (char)buf;
    }

    if(mode == 1){
        buf += vigenereKey[vigenereCounter];
        buf = buf%26;
    }else{
        if((buf - vigenereKey[vigenereCounter])<0) buf += 26;
        buf -= vigenereKey[vigenereCounter];
    }

    vigenereCounter++;
    if(vigenereCounter >= 11) vigenereCounter = 0;

    if(lowOrUp) return (char)(buf + 65);
    else return (char)(buf + 97);
}
```

here the `vigenere()` function is used to convert per character. First the inputted char would be turn into interger for easier processing, then it would be checked upon if its a lower or uppercase character since we want to change their typical ASCII values into 0-25 (A-Z); Other non-alphabetical characters would be returned immediately. Then going to the shifter, the way it is shifted is dependent on the mode that have been chosen which are either encode (1) or decode (0). This shifting is supported with `vigenereKey[]` which is a numericalized "INNUGANTENG". After which the `vigenereCounter` is incremented for future reference of the `vigenereKey[]`, this is followed by returning the characters into its ASCII Values.

```c
void vigenereFile(char *pathToDir,char *filename, int mode){
    FILE *fp1, *fp2;
    char filesrc[2048], filedup[2048],buf[1024];
    char c;
    int len = strlen(fileloc);


    for(int i=0;i<len;i++){
        buf[i] = vigenere(filename[i],mode);
    }

    sprintf(filesrc,"%s%s",pathToDir,filename);
    sprintf(filedup,"%s%s",pathToDir,buf);

    fp1 = fopen(filesrc, "r");
    if(fp1 == NULL){
        puts("Cannot open converting file.");
        exit(EXIT_FAILURE);
    }

    fp2 = fopen(filedup, "w");
    if(fp1 == NULL){
        puts("Cannot create duplicate file.");
        exit(EXIT_FAILURE);
    }

    c = vigenere(fgetc(fp1),mode);
    while(c != EOF){
        fputc(c, fp2);
        c = vigenere(fgetc(fp1),mode);
    }

    puts("Conversion successfull");
    
    fclose(fp1);
    fclose(fp2);    
    remove(fileloc);
}
```

The `vigenereFile()` function is used to convert the file's name and content with encoding/decoding of the Vigenère Cipher.

```c
void convertDirContentVigenere(char *pathToDir, int mode){
    DIR* FD;
    struct dirent* in_file;

    if (NULL == (FD = opendir (pathToDir))) 
    {
        fprintf(stderr, "Error : Failed to open input directory - %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
    while ((in_file = readdir(FD))) 
    {
        if (!strcmp (in_file->d_name, "."))
            continue;
        if (!strcmp (in_file->d_name, ".."))    
            continue;
        vigenereFile(pathToDir,in_file->d_name,mode);
    }
}
```

The `convertDirContentVigenere()` function is used to read and convert all files of the directories and encode/decode it depending on its mode.

### 2b/2c. Encode/Decode when renaming

Inside the `xmp_rename()` function that is used in FUSE, contains the snippets of code as below:

```c
 int isIANFrom, isIANTo;
    isIANFrom = checkIAN(ffrom);
    isIANTo = checkIAN(fto);
    if(isIANFrom || isIANTo){
        if(isIANFrom == isIANTo || isADir(pathToDir) == 0){
            break;
        }else if(isIANTo){
            convertDirContentVigenere(ffrom,1);
        }else{
            convertDirContentVigenere(fto,0);
        }
        systemLog(ffrom,fto,1);
    }
```

This snippet of a code is what would run when a rename action is done. Here the first thing it does is checks whether the original name (from) and the changed name (to). Then it check where the "IAN_" first came, as so this would determine if it should encode or decode, this is also where it checks if the renamed is either a file or a directory. After running `convertDirContentVigenere()`, a systemlog is created.

### 2d/2e. System Logs

The following snippet of code are used to create the logs on `home/[user]/hayolongapain_I01.log`:

```c
void logContent(char *cmd, char *desc, int infoOrWarning) 
{
    char filelogpath[1024],username[64];
    getlogin_r(username,64);
    sprintf(filelogpath,"/home/%s/hayolongapain_I01.log",username);
    time_t t = time(NULL);
    struct tm* lt = localtime(&t);
    char timestamp[30];
    strftime(timestamp, 30, "%d%m%Y-%H:%M:%S", lt);
    char content[1100];
    if(infoOrWarning){
        sprintf(content, "INFO::%s:%s::%s", timestamp, cmd, desc);
    }else{
        sprintf(content, "WARNING::%s:%s::%s", timestamp, cmd, desc);
    }
    FILE *fp = fopen(filelogpath, "a");
    fputs(content,fp);
    fclose(fp);
    return;
}

void systemLog(char *path1, char *path2, int mode) 
{
    char buff[1024], cmd[32];
    if(path2[0]!='\0' && mode == 1){
        strcpy(cmd, "RENAME");
        sprintf(buff, "%s::%s", path1, path2);
        logContent(cmd,buff,1);
    }else{
        if(mode == 2){
            strcpy(cmd, "MKDIR");
            sprintf(buff, "%s", path1);
            logContent(cmd, buff,1);
        }else if(mode == 3){
            strcpy(cmd, "RMDIR");
            sprintf(buff, "%s", path1);
            logContent(cmd, buff,0);
        }else if(mode == 4){
            strcpy(cmd, "UNLINK");
            sprintf(buff, "%s", path1);
            logContent(cmd, buff,0);
        }
    } 
    
}
```

## Revisions & Difficulties

+ Unable to run it smoothly with FUSE
+ Still have cant connect some of the functions to the FUSE entirely
+ But overall still workable.

# Soal 3
## questions
### 3a. Jika suatu direktori dibuat dengan awalan “nam_do-saq_”, maka direktori tersebut akan menjadi sebuah direktori spesial
```c
void convertDecimalToBinary(char binary[], char decimal[])
{
unsigned long long int decimalValue = 0;
int length = strlen(decimal);
for (int i = 0; i < length; i++)
{
 decimalValue *= 10;
 decimalValue += decimal[i] - '0';
}
 
length = 0;
for (length = 0; decimalValue > 0; length++)
{
 binary[length] = decimalValue % 2 + '0';
 decimalValue >>= 1;
}
 
char temp;
for (int i = 0; i < length/2; i++)
{
 temp = binary[i];
 binary[i] = binary[length - 1 - i];
 binary[length - 1 - i] = temp;
}
 
binary[length] = '\0';
}

```

```c
void convertBinerToDecimal(char decimal[], char binary[])
{
unsigned long long int decimalValue = 0;
int length = strlen(binary);
for (int i = 0; i < length; i++)
{
 decimalValue <<= 1;
 decimalValue += binary[i] - '0';
}
sprintf(decimal, "%lld", decimalValue);
}

```
if nam_do-saq_ is in the directory prefix then convert all decimal to binary using the decimaltobinary function otherwise if the directory is not special it will be converted to decimal

### 3b/c. Jika suatu direktori di-rename dengan memberi awalan “nam_do-saq_”,maka direktori tersebut akan menjadi sebuah direktori spesial. / Apabila direktori yang terenkripsi di-rename dengan menghapus “nam_do-saq_” pada bagian awal nama folder maka direktori tersebut menjadi direktori normal.
```c
static int fuse_mkdir(const char *path, mode_t mode)
{
   char filePath[1024];
   decodeDirectoryForRename(filePath, path);
  
   writeLog("INFO", "MKDIR", path, "");
  
   return mkdir(filePath, mode |S_IFDIR);
}
```
we put the convert rename function into mkdir fuse to make it a special directory

## Revisions
- fuse for directory special
- all of number 3

